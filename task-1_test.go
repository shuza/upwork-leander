package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_testValidity(t *testing.T) {
	testCases := []struct {
		desc      string
		input     string
		expResult bool
	}{
		{
			desc:      "should success",
			input:     "23-ab-48-abc-56-abcdef",
			expResult: true,
		},
		{
			desc:      "invalid number",
			input:     "abc-3j3-xyz",
			expResult: false,
		},
		{
			desc:      "invalid string",
			input:     "23-abc-abef-48",
			expResult: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			result := testValidity(tc.input)
			assert.Equal(t, tc.expResult, result)
		})
	}
}

func Test_averageNumber(t *testing.T) {
	testCase := []struct {
		desc      string
		input     string
		expResult float64
	}{
		{
			desc:      "should success",
			input:     "23-ab-48-abc-56-abcdef",
			expResult: 42.333333333333336,
		},
		{
			desc:      "should return 0 for no number",
			input:     "abcd-aaa-asdf",
			expResult: 0,
		},
		{
			desc:      "should return only valid number avg",
			input:     "23-ab-48-abc-5a6-abc-56-def",
			expResult: 42.333333333333336,
		},
	}

	for _, tc := range testCase {
		t.Run(tc.desc, func(t *testing.T) {
			result := averageNumber(tc.input)
			assert.Equal(t, tc.expResult, result)
		})
	}
}

func Test_wholeStory(t *testing.T) {
	testCases := []struct {
		desc      string
		input     string
		expResult string
	}{
		{
			desc:      "should success",
			input:     "1-hello-2-world",
			expResult: "hello world",
		},
		{
			desc:      "should return only valid world",
			input:     "1-hello-2-wor3ld",
			expResult: "hello",
		},
		{
			desc:      "should return empty string",
			input:     "1-hel3lo-2-wor3ld",
			expResult: "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			result := wholeStory(tc.input)
			assert.Equal(t, tc.expResult, result)
		})
	}
}

func Test_storyStats(t *testing.T) {
	testCases := []struct {
		desc         string
		input        string
		shortestWord string
		longestWord  string
		avgLength    float64
		roundWords   []string
	}{
		{
			desc:         "should success",
			input:        "abcdef-z-a-r-jh-bb-oiu",
			shortestWord: "a",
			longestWord:  "abcdef",
			avgLength:    2.2857142857142856,
			roundWords:   []string{"jh", "bb", "oiu"},
		},
		{
			desc:         "should return empty round words",
			input:        "aaaaa-b-a-b-zzzz",
			shortestWord: "a",
			longestWord:  "aaaaa",
			avgLength:    2.4,
			roundWords:   []string{},
		},
		{
			desc:         "shortest and longest are same",
			input:        "aaa",
			shortestWord: "aaa",
			longestWord:  "aaa",
			avgLength:    3,
			roundWords:   []string{"aaa"},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			shortestWord, longestWord, avg, words := storyStats(tc.input)
			assert.Equal(t, tc.shortestWord, shortestWord)
			assert.Equal(t, tc.longestWord, longestWord)
			assert.Equal(t, tc.avgLength, avg)
			assert.EqualValues(t, tc.roundWords, words)
		})
	}
}
