package main

import (
	"strconv"
	"strings"
)

//	difficulty = easy
//	time complexity = O(n)
//	time to implement 10 minutes
func testValidity(input string) bool {
	parts := strings.Split(input, "-")
	for _, part := range parts {
		if len(part) == 0 {
			return false
		}
		if part[0] >= '0' && part[0] <= '9' {
			if !isValidNumber(part) {
				return false
			}
		} else if !isValidString(part) {
			return false
		}
	}
	return true
}

//	difficulty = easy
//	time complexity = O(n)
//	time to implement 10 minutes
func averageNumber(input string) float64 {
	parts := strings.Split(input, "-")
	sum := 0.0
	count := 0
	for _, part := range parts {
		a, err := strconv.ParseFloat(part, 64)
		if err == nil {
			sum += a
			count++
		}
	}
	if sum == 0 {
		return 0
	}
	return sum / float64(count)
}

//	difficulty = easy
//	time complexity = O(n)
//	time to implement 10 minutes
func wholeStory(input string) string {
	result := ""
	parts := strings.Split(input, "-")
	for _, part := range parts {
		if isValidWord(part) {
			if result == "" {
				result = part
			} else {
				result = result + " " + part
			}
		}
	}
	return result
}

//	difficulty medium
//	time complexity =  O(n) + O(m) = O(n) where n is length of input string and m is number of valid words
//	time to implement 30 minutes
func storyStats(input string) (string, string, float64, []string) {
	parts := strings.Split(input, "-")
	shortestWord := input
	longestWord := ""
	words := make([]string, 0)
	sum := 0
	for _, part := range parts {
		if isValidWord(part) {
			words = append(words, part)
			if len(part) > len(longestWord) {
				longestWord = part
			}
			if len(part) < len(shortestWord) || part < shortestWord {
				shortestWord = part
			}
			sum += len(part)
		}
	}
	avgLength := float64(sum) / float64(len(words))
	roundWords := make([]string, 0)
	roundDown := sum / len(words)
	roundUp := sum / len(words)
	if sum%len(words) != 0 {
		roundUp++
	}
	for _, w := range words {
		if len(w) == roundDown || len(w) == roundUp {
			roundWords = append(roundWords, w)
		}
	}
	return shortestWord, longestWord, avgLength, roundWords
}

//	time complexity = O(n)
func isValidWord(input string) bool {
	for _, c := range input {
		if c >= '0' && c <= '9' {
			return false
		}
	}
	return true
}

//	time complexity = O(n)
//	as Atoi function will iterate over all the character and multiply to generate the number
func isValidNumber(input string) bool {
	if _, err := strconv.Atoi(input); err != nil {
		return false
	}
	return true
}

//	time complexity = O(n)
func isValidString(input string) bool {
	if len(input) == 0 {
		return true
	}
	for i := 1; i < len(input); i++ {
		a := input[i] - 1
		if input[i-1] != a {
			return false
		}
	}
	return true
}
